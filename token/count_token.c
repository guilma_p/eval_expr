/*
** count_token.c for eval_expr in /home/guilma_p//C/eval_expr
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Thu Apr  4 12:32:27 2013 pierre-jean guilmard
** Last update Thu Apr 18 06:13:42 2013 pierre-jean guilmard
*/

#include <stdlib.h>
#include "token.h"

t_head	 	*count_token(t_head *head)
{
  while (head->str[head->count])
    {
      if (isnum(head->str[head->count]))
	process_number(head);
      else if (isoperator(head->str[head->count]))
	process_operator(head);
      else if (isparentheses(head->str[head->count]))
	process_parentheses(head);
      head->count = head->count + 1;
    }
  return (head);
}
