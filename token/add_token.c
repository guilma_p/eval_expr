/*
** add_token.c for eval_expr in /home/guilma_p//C/eval_expr
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Thu Apr  4 14:32:14 2013 pierre-jean guilmard
** Last update Thu Apr 18 06:13:01 2013 pierre-jean guilmard
*/

#include <stdlib.h>
#include "token.h"

t_tok	*add_token(token_type t)
{
  t_tok	*token;

  token = malloc(sizeof(*token));
  token->data = NULL;
  token->type = t;
  token->next = NULL;
  return (token);
}
