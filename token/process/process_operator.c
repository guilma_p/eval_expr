/*
** process_operator.c for eval_expr in /home/guilma_p//C/eval_expr
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Thu Apr  4 18:44:55 2013 pierre-jean guilmard
** Last update Thu Apr 18 06:16:42 2013 pierre-jean guilmard
*/

#include "process.h"

t_head	*process_operator(t_head *head)
{
  int n;

  n = head->count;

  if (head->str[n] == '-')
    if (isoperator(head->str[n - 1]) || head->str[n - 1] == '(')
      {
	if (isnum(head->str[n + 1]))
	  {
	    head->current = check_current(head->current, &(head->root), NUMBER);
	    charcat(&(head->current->data), '-');
	    head->last = 1;
	    return (head);
	  }
	else if (head->str[n + 1] == '+' || head->str[n + 1] == '-')
	  {
	    head->current = check_current(head->current, &(head->root), NUMBER);
	    charcat(&(head->current->data), '0');
	  }
      }
  head->current = check_current(head->current, &(head->root), OPERATOR);
  charcat(&(head->current->data), head->str[n]);
  head->last = 0;
  return (head);
}
