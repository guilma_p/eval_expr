/*
** check_current.c for eval_expr in /home/guilma_p//C/eval_expr
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Fri Apr  5 01:17:47 2013 pierre-jean guilmard
** Last update Thu Apr 18 06:16:09 2013 pierre-jean guilmard
*/

#include <stdlib.h>
#include "process.h"

t_tok	*check_current(t_tok *current, t_tok **root, token_type t)
{
  if (current == NULL)
    {
      current = add_token(t);
      *root = current;
    }
  else
    {
      current->next = add_token(t);
      current = current->next;
    }
  return (current);
}
