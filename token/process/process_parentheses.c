/*
** process_parentheses.c for eval_expr in /home/guilma_p//C/eval_expr
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Thu Apr  4 18:49:27 2013 pierre-jean guilmard
** Last update Thu Apr 18 06:16:54 2013 pierre-jean guilmard
*/

#include "process.h"

t_head	*process_parentheses(t_head *head)
{
  if (head->str[head->count] == '(')
    {
      head->current = check_current(head->current, &(head->root), OPENP);
      charcat(&(head->current->data), head->str[head->count]);
    }
  else
    {
      head->current = check_current(head->current, &(head->root), CLOSEP);
      charcat(&(head->current->data), head->str[head->count]);
    }
  head->last = 0;
  return (head);
}
