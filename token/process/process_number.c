/*
** process_number.c for eval_expr in /home/guilma_p//C/eval_expr
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Thu Apr  4 17:20:20 2013 pierre-jean guilmard
** Last update Thu Apr 18 06:16:28 2013 pierre-jean guilmard
*/

#include "process.h"

t_head	*process_number(t_head *head)
{
  if (head->last == 0)
    {
      head->current = check_current(head->current, &(head->root), NUMBER);
      charcat(&(head->current->data), head->str[head->count]);
    }
  else
    charcat(&(head->current->data), head->str[head->count]);
  head->last = 1;
  return (head);
}
