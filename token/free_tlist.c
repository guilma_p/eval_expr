/*
** free_tlist.c for eval_expr in /home/guilma_p//C/day12
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Tue Apr  2 11:32:51 2013 pierre-jean guilmard
** Last update Thu Apr 18 06:13:29 2013 pierre-jean guilmard
*/

#include <stdlib.h>
#include "token.h"

void	free_tlist(t_tok *list)
{
  if (list == NULL)
    return ;
  if (list->data != NULL)
    free(list->data);
  if (list->next != NULL)
    free_tlist(list->next);
  free(list);
}
