/*
** aff_tlist.c for eval_expr in /home/guilma_p//C/day12
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Tue Apr  2 11:03:42 2013 pierre-jean guilmard
** Last update Thu Apr 18 06:13:16 2013 pierre-jean guilmard
*/

#include <stddef.h>
#include "token.h"

void		aff_tlist(t_tok *token)
{
  my_putstr("TOKEN LIST:\n");
  while (token != NULL)
    {
      my_putstr(token->data);
      my_putchar('\t');
      aff_type(token->type);
      my_putchar('\n');
      token = token->next;
    }
}

void	aff_type(token_type t)
{
  if (t == NUMBER)
    my_putstr("NUMBER");
  else if (t == OPERATOR)
    my_putstr("OPERATOR");
  else if (t == OPENP)
    my_putstr("OPEN P");
  else
    my_putstr("CLOSE P");
}
