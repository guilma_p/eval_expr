/*
** reverse_atoi.c for bistro in /home/guilma_p//C/eval_expr
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Wed Apr 17 16:26:33 2013 pierre-jean guilmard
** Last update Thu Apr 18 06:40:08 2013 pierre-jean guilmard
*/

#include <stdlib.h>
#include "misc.h"

char	*reverse_atoi(int nb, char **str)
{
  if (nb == -2147483648)
    {
      if ((*str = my_strdup("-2147483648")) == NULL)
	return (NULL);
    }
  else
    {
      if (nb < 0)
	{
	  if (charcat(str, '-') == NULL)
	    return (NULL);
	  nb = nb * -1;
	}
      if (nb / 10 != 0)
	reverse_atoi(nb / 10, str);
      if (charcat(str, (nb % 10) + '0') == NULL)
	return (NULL);
    }
  return (*str);
}
