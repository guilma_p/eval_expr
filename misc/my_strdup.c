/*
** my_strdup.c for my_strdup in /home/guilma_p//C/day07
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Wed Mar 20 12:16:56 2013 pierre-jean guilmard
** Last update Wed Apr 17 10:34:37 2013 pierre-jean guilmard
*/

#include <stdlib.h>
#include "misc.h"

char	*my_strdup(char *src)
{
  int	length;
  int	count;
  char	*dup;

  count = 0;
  if ((length = my_strlen(src)) == 0)
    return (NULL);
  if ((dup = malloc(sizeof(char) * (length + 1))) == NULL)
    return (NULL);
  while (src[count])
    {
      dup[count] = src[count];
      count += 1;
    }
  dup[count] = '\0';
  return (dup);
}
