/*
** my_putnbr.c for my_putnbr in /home/guilma_p//C/day03
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Wed Mar 13 19:03:44 2013 pierre-jean guilmard
** Last update Wed Apr 17 10:23:34 2013 pierre-jean guilmard
*/

#include "misc.h"

void	my_putnbr(int nb)
{
  if (nb == -2147483648)
    my_putstr("-2147483648");
  else
    {
      if (nb < 0)
	{
	  my_putchar('-');
	  nb = nb * -1;
	}
      if (nb / 10 != 0)
	my_putnbr(nb / 10);
      my_putchar((nb % 10) + '0');
    }
}
