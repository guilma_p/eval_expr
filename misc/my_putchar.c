/*
** my_putchar.c for ubertoolbox in /home/guilma_p//C/day03
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Wed Mar 13 09:39:42 2013 pierre-jean guilmard
** Last update Thu Apr 18 06:07:53 2013 pierre-jean guilmard
*/

#include <unistd.h>
#include "misc.h"

void	my_putchar(char c)
{
  write(1, &c, 1);
}
