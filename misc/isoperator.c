/*
** isoperator.c for isoperator in /home/guilma_p//C/lib/my
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Tue Mar 26 18:06:46 2013 pierre-jean guilmard
** Last update Thu Apr 18 06:08:56 2013 pierre-jean guilmard
*/

#include "misc.h"

int	isoperator(char c)
{
  if (c == '+')
    return (1);
  if (c == '-')
    return (1);
  if (c == '*')
    return (1);
  if (c == '/')
    return (1);
  if (c == '%')
    return (1);
  if (c == '^')
    return (1);
  return (0);
}
