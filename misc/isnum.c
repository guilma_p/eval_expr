/*
** isnum.c for isnum in /home/guilma_p//C/lib/my
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Tue Mar 26 17:55:54 2013 pierre-jean guilmard
** Last update Thu Apr 18 06:08:38 2013 pierre-jean guilmard
*/

#include "misc.h"

int	isnum(char c)
{
  if (c >= 48 && c <= 57)
    return (1);
  return (0);
}
