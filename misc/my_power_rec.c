/*
** my_power_rec.c for my_power_rec in /home/guilma_p//C/day05
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Fri Mar 15 14:53:44 2013 pierre-jean guilmard
** Last update Fri Mar 15 15:43:15 2013 pierre-jean guilmard
*/

int	my_power_rec(int nb, int power)
{
  long	my_result;
  long	my_step;

  my_result = nb;
  if (power == 1)
    return (nb);
  if (nb == 0)
    return (0);
  if (nb == 1 || power == 0)
    return (1);
  if (power < 0 || nb == -1)
    return (-1);
  my_step = my_power_rec(my_result, (power - 1));
  if (my_step == -1)
    return (-1);
  my_result = (my_result * my_step);
  if (my_result > 2147483647 || my_result < -2147483648)
    return (-1);
  else
    return (my_result);
}
