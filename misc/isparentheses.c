/*
** isparentheses.c for isparentheses in /home/guilma_p//C/lib/my
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Tue Mar 26 18:11:56 2013 pierre-jean guilmard
** Last update Thu Apr 18 06:09:11 2013 pierre-jean guilmard
*/

#include "misc.h"

int	isparentheses(char c)
{
  if (c == '(')
    return (1);
  if (c == ')')
    return (1);
  return (0);
}
