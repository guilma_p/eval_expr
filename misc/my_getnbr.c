/*
** my_getnbr.c for my_getnbr in /home/guilma_p//C/day04
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Thu Mar 14 15:01:07 2013 pierre-jean guilmard
** Last update Mon Apr  1 12:28:11 2013 pierre-jean guilmard
*/

int	my_isnbr(char);
int	my_check_limit(int*, int);
int	my_start_write(char, int*);

int	my_getnbr(char *str)
{
  int	my_count;
  int	my_nbr;
  int	my_write;
  int	my_neg;

  my_count = 0;
  my_nbr = 0;
  my_write = 0;
  my_neg = 1;
  while (*(str + my_count) != '\0')
    {
      if (my_write == 0)
	my_write = my_start_write(*(str + my_count), &my_neg);
      if (my_write == -1)
	return (0);
      if (my_isnbr(*(str + my_count)) && my_write == 1)
	if (my_check_limit(&my_nbr, (*(str + my_count) - '0')))
	  return (0);
      if (!my_isnbr(*(str + my_count)) && my_write == 1)
	return (my_nbr * my_neg);
      my_count = my_count + 1;
    }
  return (my_nbr * my_neg);
}

int	my_isnbr(char c)
{
  if (c > '9' || c < '0')
    return (0);
  return (1);
}

int	my_start_write(char c, int *my_neg)
{
  if (c == '+')
    return (0);
  else if (c == '-')
    {
      *my_neg = *my_neg * -1;
      return (0);
    }
  else if (my_isnbr(c))
    return (1);
  else
    return (-1);
}

int	my_check_limit(int *my_nbr, int my_n)
{
  long	my_int;

  my_int = *my_nbr;
  my_int = my_int * 10 + my_n;
  if ((my_int * -1) < -2147483648 && my_int > 2147483647)
    return (1);
  *my_nbr = *my_nbr * 10 + my_n;
  return (0);
}
