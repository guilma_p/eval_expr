/*
** my_ln.c for my_ln in /home/guilma_p//C/lib/my
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Wed Mar 20 11:18:52 2013 pierre-jean guilmard
** Last update Wed Apr 17 11:38:44 2013 pierre-jean guilmard
*/

#include "misc.h"

void	nl()
{
  my_putchar('\n');
}
