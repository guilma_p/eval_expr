/*
** my_strlen.c for my_strlen in /home/guilma_p//C/day04
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Thu Mar 14 11:47:09 2013 pierre-jean guilmard
** Last update Thu Apr 18 06:08:15 2013 pierre-jean guilmard
*/

#include <stdlib.h>
#include "misc.h"

int	my_strlen(char *str)
{
  int	my_count;

  if (str == NULL)
    return (0);
  my_count = 0;
  while (*(str + my_count) != '\0')
    my_count = my_count + 1;
  return (my_count);
}
