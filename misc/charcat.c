/*
** charcat.c for  in /home/guilma_p//C/eval_expr
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Thu Apr  4 14:33:26 2013 pierre-jean guilmard
** Last update Wed Apr 17 14:17:20 2013 pierre-jean guilmard
*/

#include <stdlib.h>
#include "eval_expr.h"
#include "misc.h"

char	*charcat(char **src, char c)
{
  int	count;
  char	*temp;

  count = 0;
  temp = my_strdup(*src);
  if (*src != NULL)
    free(*src);
  *src = malloc(sizeof(**src) * (my_strlen(temp) + 2));
  if (temp == NULL)
    {
      **src = c;
      *(*src + 1) = '\0';
      return (*src);
    }
  while (temp[count])
    {
      *(*src + count) = temp[count];
      count = count + 1;
    }
  *(*src + count) = c;
  *(*src + count + 1) = '\0';
  free(temp);
  return (*src);
}
