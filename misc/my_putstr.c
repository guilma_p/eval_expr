/*
** my_putstr.c for my_putstr in /home/guilma_p//C/day04
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Thu Mar 14 10:23:57 2013 pierre-jean guilmard
** Last update Wed Apr 17 10:18:32 2013 pierre-jean guilmard
*/

#include <stdlib.h>
#include "misc.h"

int	my_putstr(char *str)
{
  int	my_count;

  if (str == NULL)
    {
      my_putstr("NULL");
      return (1);
    }
  my_count = 0;
  while (*(str + my_count) != '\0')
    {
      my_putchar(*(str + my_count));
      my_count = my_count + 1;
    }
  return (0);
}
