/*
** eval_operator.c for eval_expr in /home/guilma_p//C/eval_expr
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Fri Apr  5 16:44:24 2013 pierre-jean guilmard
** Last update Thu Apr 18 05:49:49 2013 pierre-jean guilmard
*/

#include <stdlib.h>
#include "operator.h"

void		eval_operator(t_stack **stack, char *ope)
{
  char		top;
  t_stack	*s_stack;
  t_ope		*buffer;

  s_stack = *stack;
  if (s_stack->operator != NULL)
    top = s_stack->operator->data[0];
  else
    top = 0;
  if (isoperator(top))
    while (prioritarize(ope[0]) && isoperator(top))
	{
	  push_output(stack, s_stack->operator->data);
	  buffer = s_stack->operator;
	  s_stack->operator = s_stack->operator->before;
	  free(buffer);
	  if (s_stack->operator != NULL)
            top = s_stack->operator->data[0];
          else
            top = 0;
	}
      push_operator(stack, ope);
}
