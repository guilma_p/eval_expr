/*
** aff_ope.c for eval_expr in /home/guilma_p//C/eval_expr
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Fri Apr  5 15:52:18 2013 pierre-jean guilmard
** Last update Thu Apr 18 05:50:19 2013 pierre-jean guilmard
*/

#include <stdlib.h>
#include "operator.h"

void	aff_operator(t_ope *operator)
{
  my_putstr("OPERATOR:\n");
  while (operator != NULL)
    {
      my_putstr(operator->data);
      nl();
      operator = operator->before;
    }
}
