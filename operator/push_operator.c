/*
** push_operator.c for eval_expr in /home/guilma_p//C/eval_expr
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Fri Apr  5 14:59:50 2013 pierre-jean guilmard
** Last update Thu Apr 18 05:48:32 2013 pierre-jean guilmard
*/

#include <stdlib.h>
#include "operator.h"

void		push_operator(t_stack **stack, char *ope)
{
  t_stack	*s_stack;
  t_ope		*current;

  s_stack = *stack;
  if (s_stack->operator == NULL)
    {
      s_stack->operator = malloc(sizeof(*s_stack->operator));
      s_stack->operator->data = ope;
      s_stack->operator->before = NULL;
    }
  else
    {
      current = malloc(sizeof(*s_stack->operator));
      current->data = ope;
      current->before = s_stack->operator;
      s_stack->operator = current;
    }
}
