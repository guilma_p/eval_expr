/*
** free_operator.c for eval_expr in /home/guilma_p//C/eval_expr
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Fri Apr  5 15:53:21 2013 pierre-jean guilmard
** Last update Thu Apr 18 05:59:47 2013 pierre-jean guilmard
*/

#include <stdlib.h>
#include "operator.h"

void	free_operator(t_ope *operator)
{
  if (operator != NULL)
    free_operator(operator->before);
  free(operator);
}
