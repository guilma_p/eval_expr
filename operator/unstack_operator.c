/*
** unstack_operator.c for eval_expr in /home/guilma_p//C/eval_expr
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Sun Apr  7 23:02:03 2013 pierre-jean guilmard
** Last update Thu Apr 18 05:51:36 2013 pierre-jean guilmard
*/

#include <stdlib.h>
#include "operator.h"

void	unstack_operator(t_stack **stack)
{
  int	flag;
  t_ope	*buffer;

  flag = 1;
  while ((*stack)->operator != NULL && flag)
    {
      buffer = (*stack)->operator;
      if ((*stack)->operator->data[0] == '(')
	flag = 0;
      else
	push_output(stack, (*stack)->operator->data);
      (*stack)->operator = (*stack)->operator->before;
      free(buffer);
    }
}
