/*
** prioritarize.c for eval_expr in /home/guilma_p//C/eval_expr/operator
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Thu Apr 18 05:16:14 2013 pierre-jean guilmard
** Last update Thu Apr 18 06:21:01 2013 pierre-jean guilmard
*/

#include "operator.h"

int	prioritarize(char ope)
{
  if (ope != '^' && ope != '*' && ope != '/')
    return (1);
  return (0);
}
