##
## Makefile for eval_expr in /home/guilma_p//C/day13/cat
## 
## Made by pierre-jean guilmard
## Login   <guilma_p@epitech.net>
## 
## Started on  Wed Apr  3 14:09:34 2013 pierre-jean guilmard
## Last update Thu Apr 18 06:21:27 2013 pierre-jean guilmard
##

CC	= gcc

RM	= rm -f

CFLAGS	+= -Wextra -Wall -Werror
CFLAGS	+= -ansi -pedantic
CFLAGS	+= -I./include/

NAME	= eval_expr

SRCS	= main.c \
	  eval_expr.c \
	  rpn/reverse_polish_notation.c \
	  misc/charcat.c \
	  misc/my_putnbr.c \
	  misc/my_putchar.c \
	  misc/my_putstr.c \
	  misc/my_strlen.c \
	  misc/my_strdup.c \
	  misc/isnum.c \
	  misc/isoperator.c \
	  misc/isparentheses.c \
	  misc/my_power_rec.c \
	  misc/nl.c \
	  misc/my_getnbr.c \
	  misc/reverse_atoi.c \
	  head/init_head.c \
	  token/add_token.c \
	  token/aff_tlist.c \
	  token/free_tlist.c \
	  token/count_token.c \
	  token/process/check_current.c \
	  token/process/process_number.c \
	  token/process/process_operator.c \
	  token/process/process_parentheses.c \
	  opp/my_add.c \
	  opp/my_sub.c \
	  opp/my_div.c \
	  opp/my_mod.c \
	  opp/my_mul.c \
	  opp/my_pow.c \
	  opp/doop.c \
	  output/push_output.c \
	  output/aff_output.c \
	  output/free_output.c \
	  output/eval_output.c \
	  output/get_nb.c \
	  operator/push_operator.c \
	  operator/eval_operator.c \
	  operator/aff_operator.c \
	  operator/free_operator.c \
	  operator/unstack_operator.c \
	  operator/prioritarize.c \

OBJS	= $(SRCS:.c=.o)

all: $(NAME)

clean:
	$(RM) $(OBJS)

fclean: clean
	$(RM) $(NAME)

$(NAME): $(OBJS)
	 $(CC) $(OBJS) -o $(NAME)

re: fclean all

.PHONY: all clean fclean re