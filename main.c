/*
** main_test.c for main-test in /home/guilma_p//C/day04
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Thu Mar 14 09:35:48 2013 pierre-jean guilmard
** Last update Thu Apr 18 06:28:05 2013 pierre-jean guilmard
*/

#include <stdlib.h>
#include "main.h"

int	main(int ac, char **av)
{
  if (ac > 1)
    {
      my_putnbr(eval_expr(av[1]));
      my_putchar('\n');
    }
  return (0);
}
