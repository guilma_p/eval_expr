/*
** eval_expr.c for eval_expr in /home/guilma_p//C/eval_expr
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Thu Apr  4 12:11:45 2013 pierre-jean guilmard
** Last update Thu Apr 18 06:29:14 2013 pierre-jean guilmard
*/

#include <stdlib.h>
#include "eval_expr.h"

int		eval_expr(char *str)
{
  int		result;
  t_stack	*stack;

  stack = malloc(sizeof(*stack));
  stack->output = NULL;
  stack->operator = NULL;
  stack->head = init_head(str);
  stack->token = count_token(stack->head)->root;
  free(stack->head);
  result = reverse_polish_notation(stack);
  free_tlist(stack->token);
  free_output(stack->output);
  free_operator(stack->operator);
  free(stack);
  return (result);
}
