/*
** free_output.c for eval_expr in /home/guilma_p//C/eval_expr
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Fri Apr  5 12:12:57 2013 pierre-jean guilmard
** Last update Thu Apr 18 05:48:16 2013 pierre-jean guilmard
*/

#include <stdlib.h>
#include "output.h"

void	free_output(t_out *output)
{
  if (output != NULL)
    free_output(output->next);
  free(output);
}
