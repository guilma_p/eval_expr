/*
** get_nb.c for eval in /home/guilma_p//C/eval_expr/output
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Thu Apr 18 05:54:47 2013 pierre-jean guilmard
** Last update Thu Apr 18 05:59:29 2013 pierre-jean guilmard
*/

#include <stdlib.h>
#include "output.h"

int	get_nb(t_out *output)
{
  int	flag;
  int	r;

  flag = 0;
  while (output != NULL && !flag)
    {
      if (output->data != NULL)
	{
	  if (isoperator(output->data[0]) && !output->data[1])
	    flag = 0;
      	  else
	    flag = 1;
	}
      if (!flag)
	output = output->prev;
    }
  if (output == NULL)
    return (0);
  r = my_getnbr(output->data);
  output->data = NULL;
  return (r);
}
