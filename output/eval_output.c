/*
** eval_output.c for eval_expr in /home/guilma_p//C/eval_expr
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Sun Apr  7 23:00:56 2013 pierre-jean guilmard
** Last update Thu Apr 18 06:45:57 2013 pierre-jean guilmard
*/

#include <stdlib.h>
#include "output.h"

int	eval_output(t_out *output)
{
  char	*temp;
  int	a;
  int	b;
  int	r;

  r = 0;
  while (output != NULL)
    {
      temp = NULL;
      if (isoperator(output->data[0]) && !output->data[1])
	{
	  b = get_nb(output->prev);
	  a = get_nb(output->prev);
	  a = doop(a, b, output->data[0]);
	  output->data = NULL;
	  temp = reverse_atoi(a, &temp);
	  output->data = temp;
	}
      r = my_getnbr(output->data);
      output = output->next;
      free(temp);
    }
  return (r);
}
