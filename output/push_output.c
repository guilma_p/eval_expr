/*
** push_output.c for eval_expr in /home/guilma_p//C/eval_expr
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Fri Apr  5 11:13:50 2013 pierre-jean guilmard
** Last update Thu Apr 18 05:46:31 2013 pierre-jean guilmard
*/

#include <stdlib.h>
#include "output.h"

void		push_output(t_stack **stack, char *value)
{
  t_stack	*s_stack;
  t_out		*current;
  t_out		*root;

  s_stack = *stack;
  if (s_stack->output == NULL)
    {
      s_stack->output = malloc(sizeof(*s_stack->output));
      root = s_stack->output;
      s_stack->output->data = value;
      s_stack->output->next = NULL;
      s_stack->output->prev = NULL;
    }
  else
    {
      root = s_stack->output;
      while (s_stack->output->next != NULL)
	s_stack->output = s_stack->output->next;
      current = malloc(sizeof(*s_stack->output));
      current->data = value;
      current->next = NULL;
      current->prev = s_stack->output;
      s_stack->output->next = current;
    }
  s_stack->output = root;
}
