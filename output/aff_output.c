/*
** aff_output.c for eval_expr in /home/guilma_p//C/eval_expr
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Fri Apr  5 11:26:52 2013 pierre-jean guilmard
** Last update Thu Apr 18 05:47:30 2013 pierre-jean guilmard
*/

#include <stdlib.h>
#include "output.h"

void	aff_output(t_out *output)
{
  my_putstr("OUTPUT:\n");
  while (output != NULL)
    {
      my_putstr(output->data);
      my_putstr("\tprev: ");
      if (output->prev != NULL)
	my_putstr(output->prev->data);
      else
	my_putstr("NULL");
      nl();
      output = output->next;
    }
}
