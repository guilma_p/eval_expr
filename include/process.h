/*
** process.h for eval_expr in /home/guilma_p//C/eval_expr/include
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Fri Apr  5 09:39:49 2013 pierre-jean guilmard
** Last update Thu Apr 18 06:18:15 2013 pierre-jean guilmard
*/

#ifndef PROCESS_H_
# define PROCESS_H_

# include "token.h"
# include "struct.h"
# include "misc.h"

t_tok	*check_current(t_tok *, t_tok **, token_type);
t_head	*process_number(t_head *);
t_head	*process_operator(t_head *);
t_head	*process_parentheses(t_head *);

#endif /* !PROCESS_H_ */
