/*
** struct.h for eval_expr in /home/guilma_p//C/eval_expr
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Thu Apr 18 04:49:01 2013 pierre-jean guilmard
** Last update Thu Apr 18 05:39:40 2013 pierre-jean guilmard
*/

#ifndef STRUCT_H_
# define STRUCT_H_

typedef enum
{
  NUMBER = 0,
  OPERATOR = 1,
  OPENP = 2,
  CLOSEP = 3
}	token_type;

typedef struct	s_tok
{
  char 		*data;
  struct s_tok	*next;
  token_type	type;
}		t_tok;

typedef struct	s_out
{
  char 		*data;
  struct s_out	*next;
  struct s_out	*prev;
}		t_out;

typedef struct	s_ope
{
  char		*data;
  struct s_ope	*before;
}		t_ope;

typedef struct	s_head
{
  int		count;
  int		last;
  t_tok		*current;
  t_tok		*root;
  char		*str;
}		t_head;

typedef struct	s_stack
{
  t_out		*output;
  t_ope		*operator;
  t_tok		*token;
  t_head	*head;
}		t_stack;

#endif /* !STRUCT_H_ */
