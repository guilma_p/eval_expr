/*
** head.h for eval_expr in /home/guilma_p//C/eval_expr
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Thu Apr 18 04:55:23 2013 pierre-jean guilmard
** Last update Thu Apr 18 06:11:53 2013 pierre-jean guilmard
*/

#ifndef HEAD_H_
# define HEAD_H_

# include "struct.h"

t_head	*init_head(char *);

#endif /* HEAD_H_ */
