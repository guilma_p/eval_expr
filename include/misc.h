/*
** misc.h for eval_expr in /home/guilma_p//C/eval_expr/include
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Fri Apr  5 09:30:06 2013 pierre-jean guilmard
** Last update Thu Apr 18 06:39:52 2013 pierre-jean guilmard
*/

#ifndef MISC_H_
# define MISC_H_

#include "eval_expr.h"

char	*charcat(char **, char);
int	my_putstr(char *);
void	my_putchar(char);
void	my_putnbr(int);
char	*my_strdup(char *);
int	my_strlen(char *);
int	isnum(char);
int	isoperator(char);
int	isparentheses(char);
void	nl();
int	my_getnbr(char *);
char	*reverse_atoi(int, char **);

#endif /* !MISC_H_ */
