/*
** output.h for eval_expr in /home/guilma_p//C/eval_expr
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Thu Apr 18 04:58:29 2013 pierre-jean guilmard
** Last update Thu Apr 18 06:01:59 2013 pierre-jean guilmard
*/

#ifndef OUPUT_H_
# define OUTPUT_H_

# include "misc.h"
# include "struct.h"
# include "opp.h"

void	push_output(t_stack **, char *);
void	aff_output(t_out *);
void	free_output(t_out *);
int	eval_output(t_out *);
int	get_nb(t_out *);

#endif /* !OUTPUT_H_ */
