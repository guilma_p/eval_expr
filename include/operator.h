/*
** operator.h for eval_expr in /home/guilma_p//C/eval_expr
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Thu Apr 18 04:59:28 2013 pierre-jean guilmard
** Last update Thu Apr 18 05:49:30 2013 pierre-jean guilmard
*/

#ifndef OPERATOR_H_
# define OPERATOR_H_

# include "misc.h"
# include "struct.h"

void	eval_operator(t_stack **, char *);
void	push_operator(t_stack **, char *);
void	aff_operator(t_ope *);
void	free_operator(t_ope *);
int	prioritarize(char);
void	unstack_operator(t_stack **);

#endif /* !OPERATOR_H_ */
