/*
** opp.h for eval_expr in /home/guilma_p//C/eval_expr
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Fri Apr  5 01:08:01 2013 pierre-jean guilmard
** Last update Thu Apr 18 06:19:36 2013 pierre-jean guilmard
*/

#ifndef OPP_H_
# define OPP_H_

#include "misc.h"

typedef struct	s_opp
{
  char		ope;
  int		(*fct)(int, int);
}		t_opp;

int	my_add(int, int);
int	my_sub(int, int);
int	my_div(int, int);
int	my_mul(int, int);
int	my_mod(int, int);
int	my_pow(int, int);
int	doop(int, int, char);
int	my_power_rec(int, int);

#endif /* !OPP_H_ */
