/*
** eval_expr.h for eval_expr in /home/guilma_p//C/eval_expr
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Thu Apr  4 12:05:50 2013 pierre-jean guilmard
** Last update Thu Apr 18 05:43:23 2013 pierre-jean guilmard
*/

#ifndef EVAL_EXPR_H_
# define EVAL_EXPR_H_

# include "misc.h"
# include "head.h"
# include "token.h"
# include "output.h"
# include "operator.h"
# include "rpn.h"

int	eval_expr(char *);

#endif /* !EVAL_EXPR_H_ */
