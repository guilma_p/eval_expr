/*
** token.h for eval_expr in /home/guilma_p//C/eval_expr/include
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Fri Apr  5 09:34:52 2013 pierre-jean guilmard
** Last update Thu Apr 18 06:15:17 2013 pierre-jean guilmard
*/

#ifndef TOKEN_H_
# define TOKEN_H_

#include "misc.h"
#include "struct.h"
#include "process.h"

t_tok	*add_token(token_type);
void	aff_tlist(t_tok *);
void	aff_type(token_type);
t_head	*count_token(t_head *);
void	free_tlist(t_tok *);

#endif /* !TOKEN_H_ */
