/*
** rpn.h for eval_expr in /home/guilma_p//C/eval_expr
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Thu Apr 18 04:47:17 2013 pierre-jean guilmard
** Last update Thu Apr 18 05:39:08 2013 pierre-jean guilmard
*/

#ifndef RPN_H_
# define RPN_H_

#include "struct.h"

int	reverse_polish_notation(t_stack *);

#endif /* !RPN_H_ */
