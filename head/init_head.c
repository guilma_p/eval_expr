/*
** init_head.c for  in /home/guilma_p//C/eval_expr
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Thu Apr  4 18:31:12 2013 pierre-jean guilmard
** Last update Thu Apr 18 06:10:11 2013 pierre-jean guilmard
*/

#include <stdlib.h>
#include "head.h"

t_head		*init_head(char *str)
{
  t_head	*head;

  head = malloc(sizeof(*head));
  head->count = 0;
  head->last = 0;
  head->current = NULL;
  head->root = NULL;
  head->str = str;
  return (head);
}
