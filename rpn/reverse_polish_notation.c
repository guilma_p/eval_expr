/*
** reverse_polish_notation.c for eval_expr in /home/guilma_p//C/eval_expr
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Thu Apr  4 20:39:24 2013 pierre-jean guilmard
** Last update Thu Apr 18 07:19:03 2013 pierre-jean guilmard
*/

#include <stdlib.h>
#include "eval_expr.h"
#include "misc.h"
#include "rpn.h"

int	reverse_polish_notation(t_stack *stack)
{
  t_tok	*root;

  root = stack->token;
  while (root != NULL)
    {
      if (root->type == NUMBER)
	push_output(&stack, root->data);
      else if (root->type == OPERATOR)
	eval_operator(&stack, root->data);
      else if (root->type == OPENP)
	push_operator(&stack, root->data);
      else if (root->type == CLOSEP)
	unstack_operator(&stack);
      root = root->next;
    }
  unstack_operator(&stack);
  return (eval_output(stack->output));
}
