/*
** my_sub.c for eval_expr in /home/guilma_p//C/eval_expr
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Fri Apr  5 01:03:14 2013 pierre-jean guilmard
** Last update Fri Apr  5 10:40:10 2013 pierre-jean guilmard
*/

#include "opp.h"

int	my_sub(int a, int b)
{
  return (a - b);
}
