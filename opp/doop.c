/*
** doop.c for eval_expr in /home/guilma_p//C/eval_expr/output
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Thu Apr 18 05:53:33 2013 pierre-jean guilmard
** Last update Thu Apr 18 05:56:55 2013 pierre-jean guilmard
*/

#include "opp.h"

t_opp	g_opptab[] =
{
  {'+', &my_add},
  {'-', &my_sub},
  {'/', &my_div},
  {'*', &my_mul},
  {'%', &my_mod},
  {'^', &my_pow}
};

int doop(int a, int b, char op)
{
  int i;

  i = 0;
  while (g_opptab[i].ope != op)
    i += 1;
  return ((g_opptab[i].fct)(a, b));
}
