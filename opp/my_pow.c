/*
** my_pow.c for eval_expr in /home/guilma_p//C/eval_expr
** 
** Made by pierre-jean guilmard
** Login   <guilma_p@epitech.net>
** 
** Started on  Fri Apr  5 01:11:47 2013 pierre-jean guilmard
** Last update Thu Apr 18 06:19:16 2013 pierre-jean guilmard
*/

#include "opp.h"

int	my_pow(int a, int b)
{
  return (my_power_rec(a, b));
}
